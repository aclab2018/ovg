import { Template } from 'meteor/templating';

moment = require('moment');
Paho = { MQTT : require("paho-mqtt") }

moment.updateLocale("th",{
	months : ["มกราคม","กุภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","คุลาคม","พฤศจิกายน","ธันวาคม"],
	monthsShort : ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
})
NowDate = moment(new Date()).locale("th");

window.deviceStatus = {
	fan : () => $('#fan-is-active')[0].checked,
	foggy : () => $('#fog-spray-is-active')[0].checked,
	organicFertilizer : () => $('#Compound')[0].checked,
	bioProduct : () => $('#BioProduction')[0].checked,
}
window.soilStatus = { pH : 0, moisture : 0, }
window.tankStatus ={
	water : { max_level : 60, remaining : 0 },
	organicFertilizer : { max_level : 60, remaining : 0 },
	compound : { max_level : 60, remaining : 0, pH : 0, EC : 0 },
	bioProduct : { max_level : 0, remaining : 0 }
}
window.weatherStatus = {
	light : 0, temperature : 0, humidity : 0
}
// We need to confirm

var TestMQTT_client;

Template.moblie_appearance.onRendered(()=>{
	for(var i = 1; i <= 10; i++){
		$('.scale-10').append('<div class="scale"></div>');
		if(i <= 4) $('.fan').append('<div class="fan-blade"></div>');
		if(i <= 5) $('.fog-spray').append('<div class="water-stream-line"></div>');
	}
	$('#date-date').html(NowDate.format("D MMMM ")+(NowDate.get("years")+543));

	TestMQTT_client = new Paho.MQTT.Client("m11.cloudmqtt.com",Number(34822),"web_" + parseInt(Math.random() * 100, 10));

	TestMQTT_client.onMessageArrived = function(message){
		console.log(message.payloadString);
	};

	TestMQTT_client.onConnectionLost = function(error){
		console.error("Connection Lost!!");
	}
	TestMQTT_client.connect({
		useSSL : true,
		userName : "test",
		password : "1234",
		onSuccess : function(success){
			console.log('Connected.'); TestMQTT_client.subscribe("test");
		},
		reconnect :true,
	});

	/*TankStatusMQTT = new Paho.MQTT.Client("m13.cloudmqtt.com",Number(32151),"web_" + parseInt(Math.random() * 100, 10));
	TankStatusMQTT.onMessageArrived = function(message){
		// console.log(message.payloadString);
		var splitstring = message.payloadString.split(",");
		console.log(splitstring);
		tankStatus.water.remaining = parseFloat(splitstring[0])
		tankStatus.organicFertilizer.remaining = parseFloat(splitstring[2])
		tankStatus.compound.remaining = parseFloat(splitstring[4])
	};

	TankStatusMQTT.onConnectionLost = function(error){
		console.error("Connection Lost!!");
	}
	TankStatusMQTT.connect({
		useSSL : true,
		userName : "admin1234",
		password : "1234",
		onSuccess : function(success){
			console.log('TankStatus: Connected.'); TankStatusMQTT.subscribe("UltralTang");
		},
		reconnect :true,
	});*/

	
});
Template.moblie_appearance.events({
	'change #fan-is-active' : (event)=>{
		TestMQTT_client.send("test","1"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #fog-spray-is-active' : (event)=>{
		TestMQTT_client.send("test","2"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #WaterPump' : (event)=>{
		TestMQTT_client.send("test","3"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #OrganicFertilizerPump' : (event)=>{
		TestMQTT_client.send("test","4"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #Compound' : (event)=>{
		TestMQTT_client.send("test","5"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #BioProduction' : (event)=>{
		TestMQTT_client.send("test","6"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #CompoundCyclicFan' : (event)=>{
		TestMQTT_client.send("test","7"+(event.currentTarget.checked===true?"T":"F"));
	},
	'change #machine-switch' : (event)=>{
		TestMQTT_client.send("test","8"+(event.currentTarget.checked===true?"T":"F"));
	},
})